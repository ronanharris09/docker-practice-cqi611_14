import Navbar from "../components/Navbar"
import Link from "next/link"

const AboutPage = () => {
    return (
        <>
            <Navbar />
            <div className="container lg:mx-auto mx-6 flex items-center justify-center h-screen">
                <div className="block text-gray-900">
                    <h1 className="text-5xl">
                        Tentang
                    </h1>
                    <p className="text-2xl mt-3">
                        Sample CRUD App adalah sebuah frontend web yang dibangun dengan <strong>Next.js</strong> <br />
                        sebagai salah satu bentuk implementasi frontend klien dari sebuah API yang <br />
                        terhubung ke basis data mahasiswa yang sederhana dengan PostgreSQL <br />
                        <br />
                        Kunjungi halaman <Link href="/mahasiswa"><strong className="cursor-pointer">Daftar Mahasiswa</strong></Link> untuk mulai menggunakan layanan ini.
                    </p>
                </div>
            </div>
        </>
    )
}

export default AboutPage;