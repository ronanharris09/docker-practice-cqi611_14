import Navbar from "../../../components/Navbar"
import axios from 'axios'
import Link from 'next/link'

const raw_server_url = process.env.NEXT_PUBLIC_GLOBAL_BASE_URL
const raw_server_port = process.env.NEXT_PUBLIC_EXPOSED_PORT_GATEWAY_API
const global_url = `${raw_server_url}:${raw_server_port}`
const axiosInstance = axios.create({
    baseURL: global_url
})

export async function getServerSideProps(context) {
    const fetchData = await (await axiosInstance.delete(`/mahasiswa/${context.params.id}/delete`)).data
    
    console.log(fetchData)

    if (fetchData.result[0] == undefined) {
        return {
            notFound: true
        }
    }

    return {
        props: {
            fetchData,
        }
    }
}

const MahasiswaDeletePage = ({ fetchData }) => {
    return (
        <>
            <Navbar />
            <div className="container lg:mx-auto mx-6 flex-row items-center justify-center mt-24">
                <div className="block text-gray-900 text-center">
                    <h1 className="text-5xl">
                        Penghapusan Data Mahasiswa
                    </h1>
                    <p className="text-2xl mt-3">
                        Berikut ini data mahasiswa yang baru saja dihapus.
                    </p>
                </div>
                <div className="text-gray-900 border-2 border-gray-900 p-6 text-xl font-light">
                    <div className="flex my-6">
                        <label htmlFor="nama" className="font-bold w-full p-2">Nama Mahasiswa</label>
                        <input type="text" name="nama" id="nama" className="w-full p-2" required autoComplete="off" value={fetchData.result[0].mahasiswa_nama} disabled/>
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="gender" className="font-bold w-full p-2">Jenis Kelamin</label>
                        <select name="gender" id="gender" className="w-full p-2" required autoComplete="off" value={fetchData.result[0].mahasiswa_gender} disabled>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                        </select>
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="birth" className="font-bold w-full p-2">Tanggal Lahir</label>
                        <input type="date" name="birth" id="birth" className="w-full p-2" required autoComplete="off" value={fetchData.result[0].mahasiswa_birthdate} disabled />
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="period" className="font-bold w-full p-2">Periode</label>
                        <input type="number" name="period" id="period" className="w-full p-2" required autoComplete="off" value={fetchData.result[0].mahasiswa_period} disabled />
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="faculty" className="font-bold w-full p-2">Fakultas</label>
                        <select name="faculty" id="faculty" className="w-full p-2" required autoComplete="off" value={fetchData.result[0].mahasiswa_faculty} disabled >
                            <option>Computer Science</option>
                            <option>Others</option>
                        </select>
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="major" className="font-bold w-full p-2">Jurusan</label>
                        <select name="major" id="major" className="w-full p-2" required autoComplete="off" value={fetchData.result[0].mahasiswa_major} disabled >
                            <option>Teknik Informatika</option>
                            <option>Sistem Informasi</option>
                        </select>
                    </div>
                    <Link href="/mahasiswa">
                        <button id="button_delete" className="bg-gray-900 text-white text-lg font-bold p-3 hover:shadow-lg hover:bg-white hover:text-gray-900 transform transition-colors">
                            Kembali Ke Daftar Mahasiswa
                        </button>
                    </Link>
                </div>
            </div>
        </>
    )
}

export default MahasiswaDeletePage;