import Navbar from "../../components/Navbar"
import axios from 'axios'
import Link from 'next/link'
import { useState } from 'react'

const raw_server_url = process.env.NEXT_PUBLIC_GLOBAL_BASE_URL
const raw_server_port = process.env.NEXT_PUBLIC_EXPOSED_PORT_GATEWAY_API
const global_url = `${raw_server_url}:${raw_server_port}`
const axiosInstance = axios.create({
    baseURL: global_url
})

export async function getServerSideProps(context) {
    const fetchData = await (await axiosInstance.get(`/mahasiswa/${context.params.id}`)).data
    
    console.log(fetchData)

    if (!fetchData) {
        return {
            notFound: true,
        }
    }

    return {
        props: {
            fetchData
        }
    }
}

const MahasiswaDetailPage = ({ fetchData }) => {

    const [ dataNama, setDataNama ] = useState("")
    const [ dataGender, setDataGender ] = useState("")
    const [ dataBirth, setDataBirth ] = useState("")
    const [ dataPeriod, setDataPeriod ] = useState(0)
    const [ dataFaculty, setDataFaculty ] = useState("")
    const [ dataMajor, setDataMajor ] = useState("")
    
    var dataMahasiswa = {
        nama: dataNama || fetchData.result[0].mahasiswa_nama,
        gender: dataGender || fetchData.result[0].mahasiswa_gender,
        birth: dataBirth || fetchData.result[0].mahasiswa_birthdate,
        period: dataPeriod || fetchData.result[0].mahasiswa_period,
        faculty: dataFaculty || fetchData.result[0].mahasiswa_faculty,
        major: dataMajor || fetchData.result[0].mahasiswa_major,
    }

    const formSubmit = (event) => {
        event.preventDefault()

        if(!event.target.checkValidity()) {
            return
        }

        sendForm()
    }

    const sendForm = () => {
        axiosInstance({
            method: 'PUT',
            url: `/mahasiswa/${fetchData.result[0].mahasiswa_id}/update`,
            data: dataMahasiswa
        })
    }

    return (
        <>
            <Navbar />
            <div className="container lg:mx-auto mx-6 flex-row items-center justify-center mt-24">
                <div className="block text-gray-900 text-center">
                    <h1 className="text-5xl">
                        Rincian Data Mahasiswa
                    </h1>
                    <p className="text-2xl mt-3">
                        Berikut ini data lengkap untuk <strong>{ fetchData.result[0].mahasiswa_nama }</strong>
                    </p>
                </div>
                <form className="text-gray-900 border-2 border-gray-900 p-6 text-xl font-light" noValidate onSubmit={formSubmit}>
                    <div className="flex my-6">
                        <label htmlFor="nama" className="font-bold w-full p-2">Nama Mahasiswa</label>
                        <input type="text" name="nama" id="nama" className="w-full p-2" required autoComplete="off" defaultValue={fetchData.result[0].mahasiswa_nama} onChange={(e) => setDataNama(e.target.value)} />
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="gender" className="font-bold w-full p-2">Jenis Kelamin</label>
                        <select name="gender" id="gender" className="w-full p-2" required autoComplete="off" defaultValue={fetchData.result[0].mahasiswa_gender}  onChange={(e) => setDataGender(e.target.value)}>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                        </select>
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="birth" className="font-bold w-full p-2">Tanggal Lahir</label>
                        <input type="date" name="birth" id="birth" className="w-full p-2" required autoComplete="off" defaultValue={fetchData.result[0].mahasiswa_birthdate} onChange={(e) => setDataBirth(e.target.value)} />
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="period" className="font-bold w-full p-2">Periode</label>
                        <input type="number" name="period" id="period" className="w-full p-2" required autoComplete="off" defaultValue={fetchData.result[0].mahasiswa_period} onChange={(e) => setDataPeriod(e.target.value)} />
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="faculty" className="font-bold w-full p-2">Fakultas</label>
                        <select name="faculty" id="faculty" className="w-full p-2" required autoComplete="off" defaultValue={fetchData.result[0].mahasiswa_faculty} onChange={(e) => setDataFaculty(e.target.value)}>
                            <option>Computer Science</option>
                            <option>Others</option>
                        </select>
                    </div>
                    <div className="flex my-6">
                        <label htmlFor="major" className="font-bold w-full p-2">Jurusan</label>
                        <select name="major" id="major" className="w-full p-2" required autoComplete="off" defaultValue={fetchData.result[0].mahasiswa_major} onChange={(e) => setDataMajor(e.target.value)}>
                            <option>Teknik Informatika</option>
                            <option>Sistem Informasi</option>
                        </select>
                    </div>
                    <button id="button_update" className="bg-gray-700 text-white text-lg font-bold p-3 hover:shadow-lg hover:bg-white hover:text-gray-900 transform transition-colors">
                        Simpan Perubahan
                    </button>
                </form>
                <Link href={`/mahasiswa/${fetchData.result[0].mahasiswa_id}/delete`}>
                    <button id="button_delete" className="bg-gray-500 text-white text-lg font-bold p-3 hover:shadow-lg hover:bg-white hover:text-gray-900 transform transition-colors m-6">
                        Hapus Data
                    </button>
                </Link>
            </div>
        </>
    )
}

export default MahasiswaDetailPage;