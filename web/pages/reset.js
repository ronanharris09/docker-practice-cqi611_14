import Navbar from "../components/Navbar"
import axios from 'axios'
import { useState } from 'react'
import Link from 'next/link'

const raw_server_url = process.env.NEXT_PUBLIC_GLOBAL_BASE_URL
const raw_server_port = process.env.NEXT_PUBLIC_EXPOSED_PORT_GATEWAY_API
const global_url = `${raw_server_url}:${raw_server_port}`
const axiosInstance = axios.create({
    baseURL: global_url
})

const ResetPage = () => {

    const [ isReset, setIsReset ] = useState(false)

    const resetDatabase = () => {
        axiosInstance({
            method: 'DELETE',
            url: `/mahasiswa/reset`,
        })
    
        setIsReset(true)
    }

    return (
        <>
            <Navbar />
            <div className="container lg:mx-auto mx-6 flex items-center justify-center h-screen">
                <div className="block text-gray-900 text-center">
                    <h1 className="text-5xl">
                        Reset Basis Data Mahasiswa
                    </h1>
                    <p className="text-2xl mt-3">
                        Dengan Menekan Tombol Berikut, Anda Akan Mengosongkan Basis Data Mahasiswa. <br />
                        Mohon Diingat Bahwa Ini Merupakan <strong>Soft Reset</strong>.<br />
                        ID untuk data mahasiswa baru akan berlanjut dari ID terakhir sebelum reset. <br />
                        Untuk <strong>Hard Reset</strong>, ikut petunjuk pada <code>README.md</code>
                    </p>
                    {
                        isReset ?
                        <Link href="/mahasiswa">
                            <button className="bg-gray-900 text-white text-lg font-bold p-3 hover:shadow-lg hover:bg-white hover:text-gray-900 transform transition-colors">
                                Kembali Ke Daftar Mahasiswa
                            </button>
                        </Link>
                        :
                        <button id="button_reset" className="bg-gray-900 text-white text-lg font-bold p-3 hover:shadow-lg hover:bg-white hover:text-gray-900 transform transition-colors" onClick={resetDatabase}>
                            Reset 
                        </button>
                    }
                </div>
            </div>
        </>
    )
}

export default ResetPage;