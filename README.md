```
    Docs and Project are now on a stable state, please contact me for issues.
```

# Sample CRUD Web App
This is a simple example of a heavilty cut down web service architecture consisting PostgreSQL, PostgREST, Express, and Next.js + TailwindCSS + Axios all running under alpine containers with docker-compose.

Production State
| service | status |
|-|-|
| Database (PostgreSQL) | Production Ready |
| Database API (PostgREST) | Production Ready |
| Gateway API (Express) | Production Ready |
| Web Frontend (Next.js) | Production Ready |

### Microservice Flow Diagram
```bash
web_frontend -> gateway_api -> database_api -> database
```

### Default Port Mapping
| service | defaults |
|-|-|
| Database (PostgreSQL) | `5432:5432` |
| Database API (PostgREST) | `3000:3000` |
| Gateway API (Express) | `3001:3001` |
| Web Frontend (Next.js) | `80:3000` |

---
## Table of Contents
- [Techincal Requirements](#chapter-1)
- [How-to-use-this](#chapter-2)
- [API Docs](#chapter-3)
- [Documented Issues](#chapter-4)
- [Database Docs](#chapter-5)

---
### Techincal Requirements <a name="chapter-1"></a>

- Docker
- Docker Compose
- Public IP

---
### How-to-use-this <a name="chapter-2"></a>

```
DISCLAIMER : This docs presumed you're about to run this project on GNU/Linux Operating Systems. The project has been tested and working fine on Debian Buster with a server config of 1 vCore and 1GB RAM.
```

First off, clone this repo.
```bash
git clone https://gitlab.com/ronanharris09/docker-practice-cqi611_14.git
```

After that open the subfolder that contains the clone of this repo
```bash
docker-practice-cqi611_14
```

Then, open .env with your the text editor of your preference. Here I'm using vim.
```bash
vim .env
```

Inside this file, you will have to some of these lines below according to your server public IP and also the configuration of your liking.
```sh
# Ports for Containers
CONTAINER_PORT_DATABASE=5432
CONTAINER_PORT_DATABASE_API=3000
CONTAINER_PORT_GATEWAY_API=3001
CONTAINER_PORT_WEB_FRONTEND=3000

# Ports to be exposed
EXPOSED_PORT_DATABASE=5432
EXPOSED_PORT_DATABASE_API=3000
EXPOSED_PORT_GATEWAY_API=3001
NEXT_PUBLIC_EXPOSED_PORT_GATEWAY_API=3001
EXPOSED_PORT_WEB_FRONTEND=80

# Server Wide Config (MUST CHANGE)
GLOBAL_BASE_URL="http://51.158.182.184"
NEXT_PUBLIC_GLOBAL_BASE_URL="http://51.158.182.184"

# Postgres Preference
POSTGRES_DATABASE_NAME=practice_db
POSTGRES_DATABASE_SCHEMA_NAME=public
POSTGRES_ANON_USER=practice_user
POSTGRES_ANON_USER_PASSWORD="CQI611_14"
```

When you're done, save the file and then do this.
```bash
cp .env api/.env
cp .env web/.env
```

Now, it's time to start the compose script for the first time.
```bash
docker-compose up -d
```

To monitor the logs to see if every service is on a ready state, run this command below.
```bash
watch -n 1 -c docker-compose logs
```

Or you can just wait for a few minutes since it will took sometime for Next.js to finish.

To turn the containers down, run.
```bash
docker-compose down
```

And if you wanted to run this containers again run.
```bash
docker-compose build && docker-compose up -d
```

---
### API Docs <a name="chapter-3"></a>
```
ATTENTION : This chapter discuss the API design of `gateway_api` (on default port `3001`) service used by the frontend not `database_api` (on default port `3000`) which is automatically generated on the fly by PostgREST.
```
| method | path | params | desc | return example | path example |
|-|-|-|-|-|-|
| `GET` | `/mahasiswa` | none | return an array of mahasiswa with limited properties | `{result: [{mahasiswa}, {mahasiswa}]}` | `/mahasiswa` |
| `POST` | `/mahasiswa/create` | none | return an single mahasiswa object that has been created | `{result: {mahasiswa}}` | `/mahasiswa/create` |
| `DELETE` | `/mahasiswa/reset` | none | return an array of deleted mahasiswa object | `{result: [{mahasiswa}, {mahasiswa}]}` | `/mahasiswa/reset` |
| `GET` | `/mahasiswa/:mahasiswa_nim` | `:mahasiswa_nim` | return a matching mahasiswa object based on `mahasiswa_nim` with all of it's props | `{result: {mahasiswa}}` | `/mahasiswa/20180801999` |
| `PUT` | `/mahasiswa/:mahasiswa_nim/update` | `:mahasiswa_nim` | return a matching mahasiswa object based on `mahasiswa_nim` with all of it's props that has been updated | `{result: {mahasiswa}}` | `/mahasiswa/20180801999/update` |
| `DELETE` | `/mahasiswa/:mahasiswa_nim/delete` | `:mahasiswa_nim` | return a matching mahasiswa object based on `mahasiswa_nim` with all of it's props that has been removed | `{result: {mahasiswa}}` | `/mahasiswa/20180801999/delete` |

And this is the structure of a `mahasiswa` object on return.
```json
{
    mahasiswa_id: `Number`, 
    mahasiswa_nama: `String`, 
    mahasiswa_gender: `String`, 
    mahasiswa_birthdate: `Date`, 
    mahasiswa_period: `Number`, 
    mahasiswa_faculty: `String`, 
    mahasiswa_major: `String`,
    mahasiswa_created: `Timestamp`,
    mahasiswa_updated: `Timestamp`,

}
```

Although it should've been known that the same would apply, for the structure of `mahasiswa` object sent to the API, the structure required would look like this instead.
```json
{
    nama: `String`, 
    gender: `String`, 
    birth: `Date`, 
    period: `Number`, 
    faculty: `String`, 
    major: `String`,
}
```
---
### Documented Issues <a name="chapter-4"></a>
1.  **CORS**
    Gateway API employs a strict CORS rule, make sure the environment variable do refer to the correct public IP. You may also add another custom string or RegExp rule into Gateway API in `./api/src/index.js` at line 83.

2.  **Database Persistent Volume Issues**
    If you want to initialize the database from zero after using it for a while, you can either send a `DELETE` request to Gateway API at path `/mahasiswa/delete` or you can also remove the `database` folder when all of the services are in off position. The next time you turn it back on, it will reinitialize like the first time install state.

---
### Database Docs <a name="chapter-5"></a>
To know more about the structure of the table used to store mahasiswa, please refer to file `init.sql`. This file is used on each database first time initialization by mounting the file into `/docker-entrypoint-initdb.d/init.sql` which can bee seen on file `docker-compose.yml`. Further customization can be done on environment variables available on file `.env` to customize table name, database anonymous user name, etc.